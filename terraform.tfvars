####################
# Common Variables #
####################
company     = "kopicloud"
prefix      = "devops100"
environment = "dev"
location    = "germanywestcentral"
description = "Deploy a PostgreSQL Server"
owner       = "Bert Achterkamp"

##################
# Authentication #
##################
#azure-subscription-id = "complete-me"
#azure-client-id       = "complete-me"
#azure-client-secret   = "complete-me"
#azure-tenant-id       = "complete-me"

#####################
# PostgreSQL Server #
#####################
postgresql-admin-login    = "devopsadmin"
postgresql-admin-password = "Th1sIsAP@ssw0rd"
postgresql-version        = "11"
postgresql-sku-name       = "B_Gen5_1"
postgresql-storage        = "5120"
